from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

def signup_view(request):
	if request.method == "POST":
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user=form.save()
			login(request, user)
			# log the user in (cont.)
			return redirect('msg:message_list')
	else:
		form = UserCreationForm()
	return render(request, 'accounts/signup.html', {'form':form})

def login_view(request):
	if request.method =="POST":
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user=form.get_user()
			login(request,user)
			return redirect('msg:message_list')
	        # log the user in (cont.)
	else:
		form = AuthenticationForm()
	return render(request, 'accounts/login.html', {'form':form})


def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect('msg:message_list')
