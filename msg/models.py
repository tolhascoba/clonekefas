from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Message(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, )
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.author, self.snipped_message())

    def snipped_message(self):
        if len(self.message) > 20 :
            return self.message[:20] + " ..."
        return self.message

class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,)
    comment = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    message = models.ForeignKey(Message, on_delete=models.CASCADE, blank=True, null=True, related_name="comments")

    def __str__(self):
    	return "{} - {}".format(self.author, self.comment)